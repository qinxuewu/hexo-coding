---
title: SpringBoot实现的Mongodb管理工具
date: 2018-11-21 17:05:58
# 如果top值为true，则会是首页推荐文章
top: true
tags:
  - mongodb
  - springboot
categories: springboot
---



## 项目介绍
- Mongodb网页管理工具,基于Spring Boot2.0，前端采用layerUI实现。
- 源于线上环境部署mongodb时屏蔽了外网访问mongodb,所以使用不了mongochef这样方便的远程连接工具，便Mongodb提供的java api实现的的网页版管理
- 未设置登录权限相关模块，低耦合性 方便嵌入到现有的项目
## 部署文档
- https://a870439570.github.io/work-doc/mongdb

## 部分效果图如下




## 显示所有的数据源
![](http://wx1.sinaimg.cn/large/006b7Nxngy1g1g3g7qepcj31fc0nqwh9.jpg)

## 显示指定数据源下的表
![](http://wx1.sinaimg.cn/large/006b7Nxngy1g1g3gpeenij31140lhwg8.jpg)

## 源码地址

https://github.com/a870439570/Mongodb-WeAdmin

[![QQ群](https://img.shields.io/badge/QQ%E7%BE%A4-924715723-yellowgreen.svg)](https://jq.qq.com/?_wv=1027&k=5PIRvFq)
[![码云](https://img.shields.io/badge/Gitee-%E7%A0%81%E4%BA%91-yellow.svg)](https://gitee.com/qinxuewu)
[![Github](https://img.shields.io/badge/Github-Github-red.svg)](https://github.com/a870439570)



![觉得本文不错的话，分享一下给小伙伴吧~](http://wx1.sinaimg.cn/large/006b7Nxngy1g1eu6ewhl9j30760763yz.jpg)